<?php

Route::get('/', function () {
    $blogs = Post::paginate(10);
    $tags = Tag::distinct()->get(['tag']);
    return View::make('blogs')->with('blogs', $blogs)->with('tags',$tags);
});

Route::get('registration', array('before'=>'registration', function () {
    return View::make('registration');
}));

Route::post('registration',  array('before' => 'csrf',
    'uses' => 'HomeController@registration'));

Route::post('login', array('before' => 'csrf', 'uses' => 'HomeController@login'));

Route::get('logout', function(){
    Auth::logout();
    return Redirect::away('/');
});


Route::when('user/*', ['before' => 'auth | isBlocked']);

Route::resource('user', 'UserController');

Route::resource('post', 'PostController');

Route::resource('comment', 'CommentController');

Route::get('like/{id}', array('uses'=>'FavoriteController@like')
)
    ->where('id', '[0-9]+');

Route::get('unlike/{id}', array('uses'=>'FavoriteController@unlike')
)
    ->where('id', '[0-9]+');

Route::get('favorites', function () {
    return View::make('like')->with('user', Auth::user());

});

Route::get('readers', array('uses'=>'FavoriteController@readers'));

Route::get('block/{id}', array('uses'=>'BlockController@block')
)
    ->where('id', '[0-9]+');

Route::get('unblock/{id}', array('uses'=>'BlockController@unblock')
)
    ->where('id', '[0-9]+');

Route::get('blocks',  array('uses'=>'BlockController@index')
);

Route::post('send', array('uses'=>'MessageController@create')
);


Route::get('incoming', array('uses'=>'MessageController@incoming'));

Route::get('sending', array('uses'=>'MessageController@sending'));


Route::get('tag/{name}', array('uses'=>'TagController@find'))->where('name', '[a-zA-Z]+');


App::missing(function ($exception) {
    return Response::view('404', array(), 404);
});