@extends('base')

@section('content')
  @foreach ($users as $user)
    <p><a href="/user/<?php echo $user->id; ?>">{{ $user->name.' '.$user->lastname }}</a> - registered on <?php echo $user->created_at ?></p>
  @endforeach
@stop