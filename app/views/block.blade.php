@extends('user')

@section('content')
      @foreach($user->Blocks as $block)
                 <div class="blog">
                     <hr>
                     <a href="/user/{{ $block->block_id}}">{{ User::find($block->block_id)->name.' '.User::find($block->block_id)->lastname }}</a> |
                     <a href="/unblock/{{ $block->block_id}}">Unblock</a>
                     <p class="date">{{ $block->created_at }}</p>
                     <hr>
                 </div>
      @endforeach
@stop
