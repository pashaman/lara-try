@extends('base')

@section('info')
<li>#{{$tag}}</li>
@stop

@section('content')
  @foreach ($blogs as $blog)
      <div class="blog">
                         <h3><a href="/post/{{$blog->id}}">{{ $blog->record}}</a>@<a href="/user/{{$blog->user_id}}">{{$blog->name.' '.$blog->lastname}}</a></h3>
                         <p class="date">{{ $blog->created_at}}</p>
                         <hr>
                         <p>
                             {{ substr($blog->text,0,400) }}...
                         </p>
                         <hr>
      </div>
  @endforeach
    <?php echo $blogs->links() ?>

@stop