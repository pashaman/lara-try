@extends('user')

@section('content')
      <?php foreach($user->Favorites as $like) { ?>
                 <div class="blog">
                     <hr>
                     <a href="/user/{{ $like->favorite_id}}">{{ User::find($like->favorite_id)->name.' '.User::find($like->favorite_id)->lastname }}</a> |
                     <a href="/unlike/{{ $like->favorite_id}}">Unlike</a>
                     <p class="date">{{ $like->created_at }}</p>
                     <hr>
                 </div>
      <?php } ?>
@stop
