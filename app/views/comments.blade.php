@extends('user')

@section('content')
      @foreach($user->Comments as $comment)
                 <div class="blog">
                     <div class="edit_div">
                         <p><a href="/comment/{{$comment->id}}/edit">&#10000;</a> <a href="/comment/{{$comment->id}}">x</a></p>
                     </div>
                     <h3><a href="/post/{{ $comment->post_id }}">{{ $comment->Post->record}}</a>
                        @
                     <a href="/user/{{ $comment->Post->user_id }}">{{ $comment->Post->User->name }} {{ $comment->Post->User->lastname }}</a>
                     </h3>
                     <p class="date">{{ $comment->created_at }}</p>
                     <hr>
                     <p>
                        {{ $comment->comment }}
                     </p>
                     <hr>
                 </div>
      @endforeach
@stop
