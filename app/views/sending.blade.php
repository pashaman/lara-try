@extends('user')


@section('content')
  @foreach ($messages as $mess)

           <div class="message_block"><b>To <a href="/user/{{$mess->whom_id}}">{{$mess->name.' '.$mess->lastname}}</a>:</b><br>
                <small>{{$mess->created_at}}</small><br>
                &#8212; {{$mess->text}}
           </div>
           <br>
  @endforeach
@stop