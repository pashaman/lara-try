@extends('base')

@section('info')
    <div class="divider">
        <ul>
            @foreach($tags as $tag)
                <li><a href="/tag/{{$tag->tag}}">#{{$tag->tag}}</a></li>
            @endforeach
        </ul>
    </div>
@stop
@section('content')
<table class="blog" cellspacing="10">
  @foreach ($blogs as $blog)
      <tr>
        <td>
            <a href="post/{{$blog->id}}">{{ $blog->record}}</a>
        </td>
        <td>
            <a href="user/{{$blog->user_id}}">{{$blog->User->name.' '.$blog->User->lastname}}</a>
        </td><td><p class="date"> {{$blog->created_at }}</p></td>
      </tr>
  @endforeach
  </table>
    <div><?php echo $blogs->links() ?></div>

@stop