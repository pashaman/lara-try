@extends('base')

@section('content')
 <h2>Please fill out the registration form bellow</h2>
    <div class="form_container">
        {{ Form::open(array('url' => 'registration')) }}
        {{ Form::token()}}
        {{ Form::label('name', 'Name')}}
        {{ Form::text('name', Input::get('name'), ['class'=>'form'])}}
        {{ Form::label('last', 'Lastname')}}
        {{ Form::text('lastname', Input::get('lastname'), ['class'=>'form'])}}
        {{ Form::label('birthday', 'Birthday')}}
        <input type="date" name="birthday" class="form" value="<?php Input::get('birthday')?>" required/>
        {{ Form::label('email',  'E-mail')}}
        {{ Form::text('email', Input::get('email'), ['class'=>'form'])}}
        {{ Form::label('password', 'Password')}}
         {{ Form::password('password', ['class'=>'form'])}}
        {{ Form::submit('ENTER',['class'=>'button'])}}
        {{ Form::reset('RESET',['class'=>'button'])}}
        {{ Form::close() }}
    </div>
@stop
