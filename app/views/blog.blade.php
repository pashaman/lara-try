@extends('user')

@section('content')
      @foreach($user->Posts as $post)
                 <div class="blog">
                     <div class="edit_div">
                         <p><a href="/post/{{$post->id}}/edit">&#10000;</a> <a href="">x</a></p>
                     </div>
                     <h3><a href="/post/{{ $post->id }}">{{ $post->record }}</a></h3>
                     <p class="date">{{ $post->created_at }}</p>
                     <hr>
                     <p>
                        {{ substr($post->text,0,400) }}...
                     </p>
                     <hr>
                 </div>
     @endforeach
@stop
