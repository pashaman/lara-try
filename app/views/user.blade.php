@extends('layout')

@section('right_top_menu')
<div class="authorize">
      <p class="blutext"> <a href="/user/{{Auth::user()->id}}">{{Auth::user()->email}}</a>  |  <a href="/user/{{Auth::user()->id}}/edit"  class="link">Edit profile</a> |  <a href="/logout" class="link">Logout</a></p>
</div>
@stop
@section('info')
    <table width="100%">
        <tr>
             <td rowspan="3">
                <a href="/post/create">Add record</a>
             </td>
            <td>
                <img src="/resources/images/{{Auth::user()->avatar}}">
            </td>

        </tr>
        <tr>
            <td>
            {{Auth::user()->name.' '.Auth::user()->lastname}}
             </td>
        </tr>
        <tr>
            <td>
                {{Auth::user()->birthday}}
            </td>
        </tr>
    </table>
    <hr>
                 <ul>
                      <li><a href="/user/{{Auth::user()->id}}">Blog</a></li>
                      <li><a href="/readers"  class="link">Readers</a></li>
                      <li><a href="/favorites" class="link">Favorites</a></li>
                      <li><a href="/comment" class="link">Comments</a></li>
                      <li><a href="/incoming" class="link">Incoming messages</a></li>
                      <li><a href="/sending" class="link">Sending messages</a></li>
                      <li><a href="/blocks" class="link">Blocks</a></li>
                 </ul>
     <hr>
@stop
@section('content')

@stop
