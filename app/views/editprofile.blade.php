@extends('user')

@section('content')
 <h2>Please fill out the form bellow with change which you want make</h2>
    <div class="form_container">
        {{ Form::open(array('method' => 'PATCH','action' => 'user.update', 'files' => true)) }}
                {{ Form::token()}}
                {{ Form::label('name', 'Name')}}<br><br>
                {{ Form::text('name', $user->name, ['class'=>'form'])}}
                {{ Form::label('lastname', 'Lastname')}}<br><br>
                {{ Form::text('lastname', $user->lastname, ['class'=>'form'])}}
                {{ Form::label('birthday', 'Birthday')}}<br><br>
                {{--{{ Form::text('birthday', $user->birthday, ['class'=>'form'])}}--}}
                <input type="date" name="birthday" class="form" value="<?php echo $user->birthday?>" required/>
                {{Form::button('Choose image for your new avatar',['class'=>'static_button'])}}
                {{Form::file('avatar',['class'=>'hidden'])}}
                {{ Form::submit('ENTER',['class'=>'button'])}}&nbsp;
                {{ Form::reset('RESET',['class'=>'button'])}}
        {{ Form::close() }}
    </div>
@stop



