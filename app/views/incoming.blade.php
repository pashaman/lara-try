@extends('user')


@section('content')
  @foreach ($messages as $mess)

           <div class="message_block">
           From <a href="/user/{{$mess->user_id}}">{{$mess->name.' '.$mess->lastname}}</a>:<br>
                <small>{{$mess->created_at}}</small><br>
                &#8212; {{$mess->text}}
           </div>
  @endforeach
@stop