@extends('user')

@section('content')
 <h2>Create new record</h2>
    <div class="form_container">
        {{ Form::open(array('method'=>'PATCH','action' => 'post.update', 'files' => true)) }}
                {{ Form::label('record', 'Theme')}}<br><br>
                {{ Form::text('record', '', ['class'=>'form'])}}
                {{ Form::label('text', 'Record')}}
                {{ Form::textarea('text', '', ['class'=>'form'])}}<br><br>
                {{ Form::label('hash', 'Record')}}
                {{ Form::textarea('hash', '', ['class'=>'form'])}}<br><br>
                {{ Form::submit('ENTER',['class'=>'button'])}}&nbsp;
                {{ Form::reset('RESET',['class'=>'button'])}}
        {{ Form::close() }}
    </div>
@stop

