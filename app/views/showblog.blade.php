@extends('base')

@section('info')
<ul>
<br>
<br>
<br>

@foreach($blog->Tags as $tag)
<li><a href="/tag/{{$tag->tag}}">#{{$tag->tag}}</a></li>
@endforeach
</ul>
@stop

@section('content')
    <div class="blog">
        <h3>{{ $blog->record }}@<a href="/user/{{$blog->User->id}}">{{$blog->User->name}} {{$blog->User->lastname}}</a></h3>
        <p class="date">{{ $blog->created_at }}</p>
        <hr>
        <p>{{$blog->text}}</p>
        <hr>
        <a href="#" id="comment_toggle">Add comment</a>
    </div>
    <?php if(Auth::user()){?>
                <div class="comment" id="comment_toggled">
                   <div class="blog" >
                      {{ Form::open(array('method'=>'PATCH', 'action' => 'comment.update', 'files' => false)) }}
                      {{ Form::hidden('post_id', $blog->id)}}
                      {{ Form::textarea('comment', '', ['class'=>'form'])}}<br><br>
                      {{ Form::submit('ENTER',['class'=>'button'])}}&nbsp;
                      {{ Form::reset('RESET',['class'=>'button'])}}
                      {{ Form::close() }}
                    </div>
                </div>
    <?php } ?>
    @foreach($blog->Comments as $comment)
        <div class="blog">
        <a href="/user/{{$comment->User->id}}">{{$comment->User->name}} {{$comment->User->lastname}}</a>
        <p class="date">{{$comment->created_at}}</p>
        <p>{{$comment->comment}}</p>
    </div>

   @endforeach


@stop