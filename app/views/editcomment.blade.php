@extends('user')

@section('content')
 <h2>Edit comment &#171;{{$comment->comment}}&#187;</h2>
    <div class="form_container">
        {{ Form::open(array('method' => 'PATCH','action' => 'comment.update', 'files' => false)) }}
                {{ Form::hidden('id', $comment->id)}}
                {{ Form::hidden('post_id', $comment->post_id)}}
                {{ Form::hidden('user_id', Auth::user()->id)}}
                {{ Form::label('comment', 'Edit your comment')}}<br><br>
                {{ Form::textarea('comment', $comment->comment, ['class'=>'form'])}}<br><br>
                {{ Form::submit('ENTER',['class'=>'button'])}}&nbsp;
                {{ Form::reset('RESET',['class'=>'button'])}}
        {{ Form::close() }}
    </div>
@stop

