@extends('layout')

@section('right_top_menu')
<div class="authorize">
      <?php if(!Auth::user()) {?>
      <p class="blutext"><a href="registration" class="link">Registration</a>  |  <a href="#" id="log_toggle" class="link">Login</a></p>
      <?php } else {?>
            <p class="blutext"> <a href="/user"><?php echo Auth::user()->email?></a>  |  <a href="/user/{{Auth::user()->id}}/edit"  class="link">Edit profile</a> |  <a href="/logout" class="link">Logout</a></p>
     <?php }?>
</div>
@stop

@section('info')
@stop

@section('auth')
<div id="log_toggled">
   <div class="form_container" style="float: right">
       {{ Form::open(array('url' => '/login')) }}
       {{ Form::text('email', Input::get('email'),['class'=>'form'])}}
       {{ Form::password('password', ['class'=>'form'])}}
       {{ Form::submit('Login',['class'=>'button'])}}
       {{ Form::reset('Reset',['class'=>'button'])}}
       {{Form::token()}}
       {{ Form::close() }}
</div>
</div>
@stop
