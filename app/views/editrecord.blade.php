@extends('user')

@section('content')
 <h2>Edit record &#171;{{$blog->record}}&#187;</h2>
    <div class="form_container">
        {{ Form::open(array('method'=>'PATCH','action' => 'post.update', 'files' => false)) }}
                {{ Form::hidden('id', $blog->id)}}
                {{ Form::label('record', 'Theme')}}<br><br>
                {{ Form::text('record', $blog->record, ['class'=>'form'])}}
                {{ Form::label('text', 'Record')}}
                {{ Form::textarea('text', $blog->text, ['class'=>'form'])}}<br><br>
                {{ Form::label('hash', 'Record')}}
                {{ Form::textarea('hash', $blog->tags, ['class'=>'form'])}}<br><br>
                {{ Form::submit('ENTER',['class'=>'button'])}}&nbsp;
                {{ Form::reset('RESET',['class'=>'button'])}}
        {{ Form::close() }}
    </div>
@stop

