<!DOCTYPE html>
<html lang="en">
    <head>
        <META content="text/html" charset="utf-8" http-equiv="Content-Type">
        <title>Login</title>
        <link href="/resources/css/main.css" rel="stylesheet" />
        <script src="/resources/js/jquery-1.11.1.js"></script>
        <script src="/resources/js/default.js"></script>
    </head>
    <body>
        <div class="header">
       <a href="/"><h1> Blog.dev</h1></a>
            @yield('right_top_menu')
        </div>
        <div class="headline"></div>
        <div class="errors">
        </div>
       @yield('auth')
       <div class="leftmenu">
            <div class="divider">
               @yield('info')
            </div>
       </div>
        <div class="content">
         @yield('content')
        </div>
        <div class="footer">
        </div>
    </body>
</html>
