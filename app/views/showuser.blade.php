@extends('base')

@section('info')
   <ul>
       <li><img src="/resources/images/{{ $user->avatar}}"></li>
       <li> {{ $user->name.' '.$user->lastname}} <br><p class="date">{{$user->birthday}}</p></li>
       <li><a href="#" id="message_toggle">Send message</a></li>
       <li><a href="/like/{{ $user->id}}">To Favorite</a></li>
       <li><a href="/block/{{ $user->id}}">To Black List</a></li>
   </ul>
    <div class="message" id="message_toggled">
         <div class="message_container">
            {{ Form::open(array('url' => '/send')) }}
            {{ Form::hidden('whom_id', $user->id)}}
            {{ Form::textarea('text', '', ['class'=>'form'])}}<br><br>
            {{ Form::submit('ENTER',['class'=>'button'])}}&nbsp;
            {{ Form::reset('RESET',['class'=>'button'])}}
            {{ Form::close() }}
         </div>
    </div>
@stop

@section('content')
    <?php foreach($user->Posts as $blog) { ?>
           <div class="blog">
                   <h3><a href="/post/{{ $blog->id }}">{{ $blog->record }}</a></h3>
                   <p class="date">{{ $blog->created_at}}</p>
                   <hr>
                   <p>
                       {{ substr($blog->text,0,400) }}...
                   </p>
                   <hr>
           </div>
    <?php } ?>
@stop
