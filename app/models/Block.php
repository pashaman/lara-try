<?php
class Block extends Eloquent
{

    protected $table = 'blocks';
    protected $softDelete = true;


    public function user()
    {
        return $this->belongsTo('User');

    }


}
