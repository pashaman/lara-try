<?php

class Favorite extends Eloquent {

    protected $table = 'favorites';
    protected $softDelete = true;


    public function user() {
        return $this->belongsTo('User');
    }

    public function getReaders($id){

        return $this->where('favorite_id','=', $id)
                    ->join('users', 'users.id', '=','favorites.user_id')
                    ->get();
    }



}
