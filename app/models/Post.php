<?php
class Post extends Eloquent {

    protected $table = 'posts';
    protected $softDelete = true;


    public function user(){
        return $this->belongsTo('User');

    }

    public function comments(){
        return $this->hasMany('Comment');
    }

    public function tags(){
        return $this->hasMany('Tag');

    }




}
