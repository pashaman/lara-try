<?php
class BlockController extends \BaseController {

    public function index(){
        $user = User::find(Auth::user()->id);
        return View::make('block')->with('user', $user);
    }

    public function block()
    {
        $array = explode('/', $_SERVER['REQUEST_URI']);
        if (isset($array[2])) {
            $id = $array[2];
            $block = Block::where('block_id', '=', $id)->where('user_id', '=', Auth::user()->id)->get();
            if (!isset($block->id)) {
                $block = new Block();
                $block->user_id = Auth::user()->id;
                $block->block_id = $id;
                $block->save();
            }
            return Redirect::back();
        }
    }

    public function unblock()
    {
        $array = explode('/', $_SERVER['REQUEST_URI']);
        if (isset($array[2])) {
            $id = $array[2];
            Block::where('block_id', '=', $id)
                ->where('user_id', '=', Auth::user()->id)
                ->delete();
            return Redirect::back();
        }
    }
}