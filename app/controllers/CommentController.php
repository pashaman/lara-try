<?php
class CommentController extends \BaseController {

    public function __construct(){
        $this->beforeFilter('isMine');

    }

    public function index(){
        $user = User::find(Auth::user()->id);
        $user->Posts = Post::where('user_id','=', Auth::user()->id)->get();
        return View::make('comments')->with('user', $user);
    }

    public function update($id = NULL){
        $validator = Validator::make(
            Input::all(),
            array(
                'comment'   => array('required')
            )
        );
        if ($validator->passes()) {
            $id =  Input::get('id');
            if($id == NULL){
                $comment = new Comment();
            } else {
                $comment = Comment::find($id);
            }
            $comment->comment = htmlspecialchars(Input::get('comment'));
            $comment->user_id = Auth::user()->id;
            $comment->post_id = Input::get('post_id');
            $comment->save();
            return Redirect::to('/post/'.Input::get('post_id'));
        } else {
            return Redirect::to('/post/'.Input::get('post_id'));
        }
    }

    public function edit($id){
        $comment = Comment::find($id);
        return View::make('editcomment')->with('comment', $comment);

    }
}
