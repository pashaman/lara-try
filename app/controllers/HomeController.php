<?php

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function showWelcome()
    {
        return View::make('hello');
    }



    public function login() {

        $email = Input::get('email');
        $password = Input::get('password');
        if (Auth::attempt ( array( 'email' =>$email, 'password' => $password ) ) ) {
            return Redirect::back();
        } else {
            return Redirect::back(302);
        }

    }

    public function registration()
    {
        $validator = Validator::make(
            Input::all(),
            array(
                'name' => array('required', 'alpha'),
                'lastname' => array('required', 'alpha'),
                'email' => array('required', 'email', 'unique:users'),
                'password' => array('required'),
                'birthday' => array('required'),
            )
        );
        if ($validator->passes()) {
            $user = new User();
            $user->name = Input::get('name');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->birthday = Input::get('birthday');
            $user->password = Hash::make(Input::get('password'));
            $user->avatar = 'default.png';
            $user->save();
            Auth::login($user);
            return Redirect::to('/user')->with(
                'success',
                'Welcome to the site,' . Auth::user()->name . '!'
            );

        } else {
            return Redirect::to('registration')->with(
                'error',
                'Please correct the following errors:'
            )->withErrors($validator);
        }
    }

}
