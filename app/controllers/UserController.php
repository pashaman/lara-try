<?php

class UserController extends \BaseController {
    public function __construct()
    {
        $this->beforeFilter('auth | isBlocked');
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->afterFilter('log', array('only' =>
            array('fooAction', 'barAction')));
    }


    public function index()
    {
        $user=User::find(Auth::user()->id);
        return View::make('blog')->with('user', $user);
    }




    public function create()
    {

    }



    public function store()
    {
    }



    public function show($id = NULL) {


        if($id == NULL){
            $user = Auth::user();
        } else {
            $user = User::findOrFail($id);
        }
        if($user == Auth::user()){
            return View::make('blog')->with('user', Auth::user());

        }
        return View::make('showuser')->with('user', $user);
    }


    public function edit()
    {
        $user = User::find(Auth::user()->id);
        return View::make('editprofile')->with('user', $user);
    }

    public function update()
    {
        $validator = Validator::make(
            Input::all(),
            array(
                'name' => array('required', 'alpha'),
                'lastname' => array('required', 'alpha'),
                'birthday' => array('required', 'date'),
                'avatar' => 'image'
            )
        );
        $messages = get_object_vars($validator->messages());
        if ($validator->passes()) {
            $user = Auth::user();
            $user->name = Input::get('name');
            $user->lastname = Input::get('lastname');
            $user->birthday = Input::get('birthday');
            if (Input::file('avatar')) {
                Input::file('avatar')->move('resources/images/', $user->id . '.' . 'png');
                $user->avatar = $user->id . '.' . 'png';
            }
            $user->save();
            return Redirect::to('/user')->with(
                'success',
                'Profile was successfully edit,' . Auth::user()->name . '!'
            );
        } else {
            return View::make('editprofile', $messages)->with('user', Auth::user());
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
