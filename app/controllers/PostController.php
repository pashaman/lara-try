<?php

class PostController extends \BaseController {

    public function __construct()
    {
        $this->beforeFilter('isMine');

    }

	public function create()
	{
        return View::make('addrecord')->with('user', Auth::user());
	}

	public function show($id)
	{
        $blog = Post::findOrFail($id);
        return View::make('showblog')->with('blog', $blog);
	}


	public function edit($id)
	{
        $blog = Post::findOrFail($id);
        $tags = Tag::where('post_id','=',$id)->get();
        $string = '';
        foreach($tags as $tag){
            $string .= '#'.$tag->tag;
        }
        $blog->tags = $string;
        return View::make('editrecord')->with('blog', $blog);
	}



	public function update($id)
	{
        $validator = Validator::make(
            Input::all(),
            array(
                'record' => array('required'),
                'text'   => array('required')
            )
        );
        if ($validator->passes()) {
            $blog = Post::find(Input::get('id'));
            if(!$blog){
                $blog = new Post();
            }
            $blog->record = htmlspecialchars( Input::get('record'));
            $blog->text = htmlspecialchars(Input::get('text'));
            $blog->user_id = Auth::user()->id;
            $blog->save();
            $tags = Tag::where('post_id','=',Input::get('id'))->get();
            foreach($tags as $tag){
                $position = strpos(Input::get('hash'), $tag->tag);
               if($position === FALSE){
                   Tag::where('id','=',$tag->id)->delete();
               }
            }
            $array = explode('#', Input::get('hash'));
            unset($array[0]);
            foreach($array as $i){
                $tag_array = explode(' ', $i);
                if (count($tag_array) != 1 || trim($i) == "" ) {
                } else {
                    $tag = Tag::where('tag', '=', $i)->where('post_id', '=', $blog->id)->first();
                    if (!$tag) {
                        $tag = new Tag();
                        $tag->tag = $i;
                        $tag->post_id = $blog->id;
                        $tag->save();
                    }
                }
            }

            return Redirect::to('user');

        } else {
            $messages = $validator->messages();
            return View::make('editrecord', $messages);

        }
	}




	public function destroy($id)
	{
        $this->beforeFilter('isMine');
        $blog = Post::find($id);
        $blog->delete();
        return Redirect::back();
	}


}
