<?php

class MessageController extends \BaseController {


    public function sending(){
            $sending = Message::join('users','users.id','=','messages.whom_id')
            ->where('user_id','=', Auth::user()->id)
            ->get();

        return View::make('sending')->with('messages', $sending);
    }

    public function incoming(){
        $incoming = Message::join('users','users.id','=','messages.user_id')
            ->where('whom_id','=', Auth::user()->id)
            ->get();
        return View::make('incoming')->with('messages', $incoming);

    }

    public function create()
    {
        $validator = Validator::make(
            Input::all(),
            array(
                'whom_id' => array('required', 'integer'),
                'text' => array('required'),
            )
        );

        if ($validator->passes()) {
            $mess = new Message();
            $mess->user_id = Auth::user()->id;
            $mess->whom_id = Input::get('whom_id');
            $mess->text = htmlspecialchars(Input::get('text'));
            $mess->save();
        }
        return Redirect::back();
    }


}
