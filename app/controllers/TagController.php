<?php
class TagController extends \BaseController {

    public function find(){
        $array = explode('/',$_SERVER['REQUEST_URI']);
        $name  = $array[2];
        $blogs = Tag::select(['users.name','users.lastname','posts.user_id','posts.record','posts.id','posts.text' ])->where('tag','=', $name)->join('posts','tags.post_id','=','posts.id')->join('users','users.id','=','posts.user_id')->paginate(10);
        return View::make('tag')->with('blogs', $blogs)->with('tag', $name);


    }


}
