<?php
class FavoriteController extends \BaseController {

    public function like()
    {
        $array = explode('/', $_SERVER['REQUEST_URI']);
        if (isset($array[2])) {
            $id = $array[2];
            $favorite = Favorite::where('favorite_id', '=', $id)->where('user_id', '=', Auth::user()->id)->get();
            if (!isset($favorite->id)) {
                $favorite = new Favorite();
                $favorite->user_id = Auth::user()->id;
                $favorite->favorite_id = $id;
                $favorite->save();
            }
            return Redirect::back();
        }
    }

    public function unlike()
    {
        $array = explode('/', $_SERVER['REQUEST_URI']);
        if (isset($array[2])) {
            $id = $array[2];
            Favorite::where('favorite_id', '=', $id)
                ->where('user_id', '=', Auth::user()->id)
                ->delete();
            return Redirect::back();
        }
    }

    public function readers(){
        $favorite = new Favorite();
        $user = Auth::user();
        $user->Readers = $favorite->getReaders(Auth::user()->id);
        return View::make('readers')->with('user', $user);
    }
}