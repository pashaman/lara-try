<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    //
});


App::after(function($request, $response)
{
    //
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest())
    {
        if (Request::ajax())
        {
            return Response::make('Unauthorized', 401);
        }
        else
        {
            return Redirect::guest('/');
        }
    }
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() !== Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});


Route::filter('registration', function()
{
    if (Auth::check()){
        return Redirect::to('user');
    }
});



Route::filter('isMine', function()
{
    $some = explode('/',$_SERVER['REQUEST_URI']);
    if(count($some) > 3){
        $array = explode('/',$_SERVER['REQUEST_URI']);
        $obj = ucfirst($array[1]);
        $obj = new $obj();
        if ($obj->find($array[2])->user_id != Auth::user()->id || Auth::user()->is_admin != 1){
            return Redirect::to('user');
        }
    }

});

Route::filter('isBlocked', function($route, $request, $response = NULL ){
    $array = explode('/', $_SERVER['REQUEST_URI']);
    if($array[1]=='user' && isset($array[2]) && Auth::user()->is_admin != 1){
        $id = $array[2];
        $blocked_me = Block::select('user_id')
            ->where('block_id', '=', Auth::user()->id)
            ->get()
            ->lists('user_id');
        foreach($blocked_me as $i){
            if ($i == $id) {
                return Redirect::away('/user')->with('message','User has restrict access to humself');
            }
        }
    }
});

Route::filter('isAdmin', function(){
    if(Auth::user()->is_admin != '1'){
        return Redirect::away('/user')->with('message','Permission denied');
    }

});


